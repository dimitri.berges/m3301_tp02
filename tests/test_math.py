import unittest
from maths import *

class TestMathsFunctions(unittest.TestCase):

    def test_multiply(self):
        self.assertEqual(multiply(2, 2), 4)
        self.assertEqual(multiply(3, 7), 21)
    def test_divide(self):
        self.assertEqual(divide(2, 2), 1)
        self.assertEqual(divide(12, 3), 4)
    def test_add(self):
        self.assertEqual(add(2, 2), 4)
        self.assertEqual(add(3, 7), 10)
    def test_rm(self):
        self.assertEqual(rm(2, 2), 0)
        self.assertEqual(rm(3, 7), -4)